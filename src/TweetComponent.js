import React from 'react';
import {Tweet} from "./Tweet";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import Divider from '@material-ui/core/Divider';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography'
import {Button} from "@material-ui/core";


export class TweetComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tweet: new Tweet(props,0,'',0)
        };

        this.handleOnClick = this.handleOnClick.bind(this);
    }

    handleOnClick() {
        this.state.tweet.likes++;
        this.props.motherObject.forceUpdate();
    }

    render() {
        this.state.tweet = this.props.tweet;
        return  (
            <div>
            <ListItem alignItems="flex-start">
                <ListItemText secondary={
                    <React.Fragment>
                        <Typography
                            component="span"
                            variant="body2"
                            color="textPrimary"
                            >Author: </Typography>
                        {this.state.tweet.toString()}
                    </React.Fragment>
                }  />
            </ListItem>
                <Button variant="contained" color="secondary" onClick={this.handleOnClick}>LIKE  {this.state.tweet.likes}</Button>
                <Divider variant="inset" component="li" /></div>
        );
    }
}