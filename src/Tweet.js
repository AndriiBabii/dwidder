import React from 'react';

export class Tweet {
    constructor(id, text, likes) {
        this._id = id;
        this._likes = likes;
        this._text = text;
    }

    get text() {
        return this._text;
    }

    set text(value) {
        this._text = value;
    }

    get likes() {
        return this._likes;
    }

    set likes(value) {
        this._likes = value;
    }

    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }

    toString() {return this._text}

}