import React from 'react';
import {Tweet} from './Tweet'
import {TweetComponent} from "./TweetComponent";
import {NewTweet} from "./NewTweet";
import List from '@material-ui/core/List';
import {container} from "./App.js"
import ListItem from '@material-ui/core/ListItem';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    inline: {
        display: 'inline',
    },
}));


export class MainPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tweets: [],
            counter: 0
        };
    }

    like() {}


    /*handleOnClickCreate(e) {
        const txt = e.target.value;
        this
    }*/

    triggerUpdating() {this.setState();}

    handleCounter() {
        this.setState({counter: this.state.counter + 1});
    }

    render() {
        var txt = '';
        const columnOfTweets = this.state.tweets.map(tweet => <li key={tweet.id}><TweetComponent tweet={tweet} motherObject={this}/></li>)
        return (
            <div>
                <NewTweet tweets={this.state.tweets} id={this.state.counter} motherObject={this}/>
                <div>
                    <List>
                        {columnOfTweets}
                    </List>
                </div>
            </div>
        );
    }
}