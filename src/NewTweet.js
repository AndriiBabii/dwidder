import React from 'react';
import {Tweet} from "./Tweet";
import {container} from "./App.js"
import Button from "@material-ui/core/Button";

export class NewTweet extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            text: ""
        };
        this.handleChange = this.handleChange.bind(this);
        this.handlerSubmit = this.handlerSubmit.bind(this);
    }

    handleChange(e) {
        const text = e.target.value;
        this.setState({
            text: text
        });
    }

    handlerSubmit(e) {
        e.preventDefault();
        const text = this.state.text;
        console.log("New Tweet: ", text);

        this.props.tweets.unshift(new Tweet(this.props.id,text,0));
        this.props.motherObject.handleCounter();
        this.props.tweets.forEach(each => console.log(each.toString()))

        this.setState({
            text: ''
        });
    }

    render() {
        const text = this.state.text;
        return (
            <div>
            <form className="new-tweet" onSubmit={this.handlerSubmit}>
                     <textarea
                         placeholder="What's happenning"
                         value={text}
                         onChange={this.handleChange}
                         className="textarea"
                         maxLength={560}
                         style={{
                             width: "100%",
                             height: 200,
                             border: "1px solid black",
                             display: "flex",
                             top: 0,
                             alignItems: "center"
                         }}
                     />
                <Button className="btn" type="submit" disabled={text === ""}>
                    Submit
                </Button>
            </form>
            </div>
        );
    }

}

// <input type="text" value={txt}/>
// <br/>
// <button onClick={this.create(txt)}>Create</button>
// <button onClick={this.like}>Like</button>